"""This module contains decorators used in the project"""

from functools import wraps
from inspect import signature
import time


DEFAULT_ATTEMPT_COUNT = 5
FUNCTION_IS_NOT_EXECUTED_MESSAGE = 'Function has not been executed due to following exception:'

def try_decorator(function):
    """Decorator that executes any function several times with try clause. If try fails then it waits 
    some amount of time and executes function again until reaches attempt_count number of tries.

    Parameters
    ----------
    function : Python function
        A function, which is wrapped by try_decorator. If function has an attempt_count argument specified, then
        it is passed to try_decorator. Otherwise, try_decorator executes default number of tries
    """

    @wraps(function)
    def inner_function(*args, **kwargs):        
        attempt_count = _get_attempt_count(function, args, kwargs)
        result = _execute_function_with_try_clause(function, args, kwargs, attempt_count)
        return result
    return inner_function

def _get_attempt_count(function, args, kwargs):
    function_arguments_length = len(signature(function).parameters)
    # If all arguments are provided it means that attempt_count is provided as well and we need to get its value
    if len(args) == function_arguments_length:
        # In all functions it is last argument
        attempt_count = args[-1]
    elif 'attempt_count' in kwargs.keys():
        attempt_count = kwargs['attempt_count']
    else:
        attempt_count = DEFAULT_ATTEMPT_COUNT
    return attempt_count

def _execute_function_with_try_clause(function, args, kwargs, attempt_count):
    result = None
    function_is_executed = False
    exception = None
    for attempt_num in range(attempt_count):
        try:
            result = function(*args, **kwargs)
            function_is_executed = True
            break
        except Exception as e:
            exception = e
            time.sleep(attempt_num + 1)
            continue
    if function_is_executed == False:
        print(FUNCTION_IS_NOT_EXECUTED_MESSAGE)
        print(exception)
    return result