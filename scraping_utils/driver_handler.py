"""This module contains Driver Handlers, which allow to launch and kill drivers (browsers) locally or within Docker."""

from abc import ABC, abstractmethod
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from . import utils


GET_ACTIVE_PORTS_COMMAND = 'netstat -tulpn | grep tcp6'
GET_DOCKER_LOGS_COMMAND = 'docker logs {docker_name}_{port}'
KILL_DOCKER_COMMAND = 'docker container rm -f {docker_name}_{port}'

BUSY_PORT_MESSAGE = 'Port {port} is already in use. Please choose other one.'
DOCKER_FAILED_TO_LAUNCH_MESSAGE = 'Docker has failed to launch'

DOCKER_LAUNCH_WAIT_TIME_SECONDS = 120  
DEFAULT_PORT = 4444

class BaseDriverHandler(ABC):
    """Base class that all Driver Handlers are derived from.

    Methods
    -------
    launch()
        Launches a driver.
    set_driver_options(options_args, options)
        Forms an object that keeps driver options
    launch_docker()
        Launches driver within a docker
    kill_docker()
        Kills previously launched docker
    """

    @abstractmethod
    def launch(self):
        """Abstract method which launches a driver. Every Driver Handler must implement this method"""
        raise NotImplementedError
    
    def set_driver_options(self, options_args: list, options: Options) -> Options:
        """Forms an object that keeps driver options.

        Parameters
        ----------
        options_args : list
            A list containing driver options that should be set.
        options: selenium.webdriver.chrome.options.Options
            An empty object, which will keep defined options
        
        Returns
        -------
        selenium.webdriver.chrome.options.Options
            An Options object, which keeps defined options
        """
        for argument in options_args:
            options.add_argument(argument)
        return options

    def launch_docker(self) -> None:
        """Launches a docker with driver inside.
        
        Raises
        ------
        ConnectionError
            If chosen port is already in use
        TimeoutError
            If Docker is failed to launch within specified time
        """
        if self.port:
            self._check_if_port_is_available()
        else:
            self._find_empty_port()
        utils.execute_bash_command(self.launch_docker_command.format(docker_name=self.docker_name, port=self.port))
        self._wait_till_docker_is_ready()
    
    def kill_docker(self) -> None:
        """Kills already launched docker"""
        utils.execute_bash_command(KILL_DOCKER_COMMAND.format(docker_name=self.docker_name, port=self.port))

    def _check_if_port_is_available(self) -> None:
        active_ports = utils.execute_bash_command(GET_ACTIVE_PORTS_COMMAND)
        if str(self.port) in active_ports:
            raise ConnectionError(BUSY_PORT_MESSAGE.format(port=self.port))

    def _find_empty_port(self):
        active_ports = utils.execute_bash_command(GET_ACTIVE_PORTS_COMMAND)
        port = DEFAULT_PORT
        while True:
            if str(port) not in str(active_ports):
                break
            else:
                port += 1
        self.port = port

    def _wait_till_docker_is_ready(self):
        docker_is_ready = False
        for i in range(DOCKER_LAUNCH_WAIT_TIME_SECONDS):
            docker_logs = utils.execute_bash_command(GET_DOCKER_LOGS_COMMAND.format(docker_name=self.docker_name,
                                                                                    port=self.port))
            if self.docker_is_ready_message in docker_logs:
                docker_is_ready = True
                break
            else:
                time.sleep(1)
                continue
        if docker_is_ready == False:
            raise TimeoutError(DOCKER_FAILED_TO_LAUNCH_MESSAGE)
    

CHROME_DEFAULT_OPTIONS_ARGS = [
    '--disable-extensions',
    '--disable-gpu',
    '--no-sandbox',
    '--disable-dev-shm-usage',
]
LAUNCH_CHROME_DOCKER_COMMAND = 'docker run --name {docker_name}_{port} -d -p {port}:4444 selenium/standalone-chrome'
CHROME_DOCKER_IS_READY_MESSAGE = 'Selenium Server is up and running'
DOCKER_NAME = 'chrome_driver'
CHROME_DRIVER_URL = 'http://localhost:{port}/wd/hub'
DEFAULT_WINDOW_SIZE = (1024, 768)

class ChromeDriverHandler(BaseDriverHandler):
    r"""Class that launches and closes Chrome Driver.

    Attributes
    ----------
    headless: bool, default=True
        Controls whether driver is launched in headless or headfull mode
    incognito: bool, default=True
        Controls whether driver is launched in incognito mode
    launch_within_docker: bool, default=False
        If True launches driver within Docker
    port: int, default=None
        Is evaluated only if driver is launched within a Docker. If None, then Docker is launched on first available port,
        otherwise it is launched on specified port

    Methods
    -------
    launch()
        Launches Chrome driver
    close()
        Closes Chrome driver
    """

    def __init__(self, headless: bool=True, incognito: bool=True, launch_within_docker: bool=False, port: int=None) -> None:
        self.headless = headless
        self.incognito = incognito

        self.launch_within_docker = launch_within_docker
        self.port = port
        self.docker_name = DOCKER_NAME
        self.launch_docker_command = LAUNCH_CHROME_DOCKER_COMMAND
        self.docker_is_ready_message = CHROME_DOCKER_IS_READY_MESSAGE
        self.driver_url = CHROME_DRIVER_URL

        self.driver = None
    
    @property
    def driver_options_args(self):
        options_args = CHROME_DEFAULT_OPTIONS_ARGS
        if self.headless:
            options_args += ['--headless']
        if self.incognito:
            options_args += ['--incognito']
        return options_args

    def launch(self) -> None:
        """Launches Chrome driver."""
        options = webdriver.ChromeOptions()
        options = self.set_driver_options(self.driver_options_args, options)
        if self.launch_within_docker:
            self.launch_docker()
            driver = webdriver.Remote(self.driver_url.format(port=self.port), options.to_capabilities())
        else:
            driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
        driver.set_window_size(DEFAULT_WINDOW_SIZE[0], DEFAULT_WINDOW_SIZE[1])
        self.driver = driver
        
    def close(self) -> None:
        """Closes Chrome driver."""
        if self.launch_within_docker:
            self.kill_docker()
        else:
            self.driver.close()
        self.driver = None
    


    
    
        


        
            
        

        