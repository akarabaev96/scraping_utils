"""This module contains web page interaction utils used throughout the project."""

import time
from typing import Union, List, Tuple

from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.chrome.webdriver import WebDriver

from . import decorators


WEB_ELEMENT_INPUT_UNKNOWN_TYPE_MESSAGE = 'Unknown web element input type. Input should be string, which is xpath of an element,' \
                                          + ' or Selenium WebElement.'

def check_element_existence(driver: WebDriver,
                            element: Union[str, WebElement]) -> bool:
    r"""Checks if element exists on current web page.
    
    Parameters
    ----------
    driver: selenium.webdriver.chrome.webdriver.Webdriver
        Selenium driver in a launched state
    element: str or selenium.webdriver.remote.webelement.WebElement
        Element's xpath or element object itself
    
    Returns
    -------
    bool
        True if element exists on current web page, otherwise False
    """

    element = _get_element_from_input(driver, element)
    existence_status = True if element else False
    return existence_status
            

@decorators.try_decorator
def click_element(driver: WebDriver, 
                  element: Union[str, WebElement], 
                  attempt_count: int = None) -> None:
    r"""Navigates to an element and then clicks it. Fully imitates user behavior.
    
    Parameters
    ----------
    driver: selenium.webdriver.chrome.webdriver.Webdriver
        Selenium driver in a launched state
    element: str or selenium.webdriver.remote.webelement.WebElement
        Element's xpath or element object itself
    attempt_count: int, default=None
        Controls how many times try_decorator will be executed. If None, then try_decorator uses default number
        of attempts
    """

    element = _get_element_from_input(driver, element)
    driver.execute_script('arguments[0].scrollIntoView();', element)
    ActionChains(driver).move_to_element(element).click().perform()
        

@decorators.try_decorator
def get_elements_data(driver: WebDriver, 
                      xpath: str, 
                      attribute: str,
                      attempts_count: int = None) -> List[str]:
    r"""Retrieves data of all elements having specified path and attribute.
    
    Parameters
    ----------
    driver: selenium.webdriver.chrome.webdriver.Webdriver
        Selenium driver in a launched state
    xpath: str
        Elements' xpath used to locate them
    attribute: str
        Elements' attribute to retrieve
    attempt_count: int, default=None
        Controls how many times try_decorator will be executed. If None, then try_decorator uses default number
        of attempts

    Returns
    -------
    List
        List containing all elements' data
    """

    elements_data = []
    elements = driver.find_elements_by_xpath(xpath)
    for element in elements:
        ActionChains(driver).move_to_element(element).perform()
        elements_data.append(element.get_attribute(attribute))
    return elements_data


@decorators.try_decorator
def scroll_page_to_bottom(driver: WebDriver,
                          scroll_steps: int = 1, 
                          sleep_time_between_steps: int = 0.3,
                          attempts_count: int = None) -> None:
    r"""Scroll page to bottom.
    
    Parameters
    ----------
    driver: selenium.webdriver.chrome.webdriver.Webdriver
        Selenium driver in a launched state
    scroll_steps: int, default=1
        Controls in how many steps page will be scrolled to bottom. If more than 1 it means that page will be
        scrolled smoothly. Useful for dynamically loaded pages
    sleep_time_between_steps: int=0.3
        Controls how many seconds to wait between scrolling steps
    attempt_count: int, default=None
        Controls how many times try_decorator will be executed. If None, then try_decorator uses default number
        of attempts
    """
    
    for scroll_step_num in range(scroll_steps):
        driver.execute_script(f'window.scrollTo(0, document.body.scrollHeight * {(scroll_step_num + 1) / scroll_steps})')
        time.sleep(sleep_time_between_steps)

@decorators.try_decorator
def scroll_page_to_specific_location(driver: WebDriver,
                                     location: float,
                                     attempts_count: int = None) -> None:
    """Scrolls page to specific location.

    Parameters
    ----------
    driver: selenium.webdriver.chrome.webdriver.Webdriver
        Selenium driver in a launched state
    location: float
        Float specifying to which part of page scroll to. 0 - top, 0.5 - middle and 1 - bottom of a page
    """

    driver.execute_script(f'window.scrollTo(0, document.body.scrollHeight * {location})')


@decorators.try_decorator
def scroll_element(driver: WebDriver, 
                    scrollable_element: Union[str, WebElement], 
                    scroll_steps: int = 1, 
                    sleep_time_between_steps: int = 0.3,
                    attempts_count: int = None) -> None:
    r"""Scrolls a scrollable element.

    Parameters
    ----------
    driver: selenium.webdriver.chrome.webdriver.Webdriver
        Selenium driver in a launched state
    scrollable_element: str or selenium.webdriver.remote.webelement.WebElement
        Scrollable element's xpath or element object itself
    scroll_steps: int, default=1
        Controls in how many steps element will be scrolled to bottom. If more than 1 it means that element will be
        scrolled smoothly. Useful for dynamically loaded pages
    sleep_time_between_steps: int=0.3
        Controls how many seconds to wait between scrolling steps
    attempt_count: int, default=None
        Controls how many times try_decorator will be executed. If None, then try_decorator uses default number
        of attempts
    """
    
    scrollable_element = _get_element_from_input(driver, scrollable_element)
    for scroll_step_num in range(scroll_steps):
        driver.execute_script(f'arguments[0].scrollTop = arguments[0].scrollHeight * {(scroll_step_num + 1) / scroll_steps}')
        time.sleep(sleep_time_between_steps)
    

@decorators.try_decorator
def write_inside_element(driver: WebDriver,
                         element: Union[str, WebElement],
                         text: str,
                         attempt_count: int = None) -> None:
    r"""Writes specified text inside element.

    Parameters
    ----------
    driver: selenium.webdriver.chrome.webdriver.Webdriver
        Selenium driver in a launched state
    element: str or selenium.webdriver.remote.webelement.WebElement
        Element's xpath or element object itself
    text: str
        Text to write inside element
    attempt_count: int, default=None
        Controls how many times try_decorator will be executed. If None, then try_decorator uses default number
        of attempts
    """

    element = _get_element_from_input(driver, element)
    # Clean everything before writing
    element.send_keys(Keys.CONTROL + 'a')
    element.send_keys(Keys.DELETE)

    element.send_keys(text)


@decorators.try_decorator
def get_select_data(driver: WebDriver,
                    element: Union[str, WebElement],
                    attempt_count: int = None) -> Tuple[WebElement, List[str]]:
    """Returns select WebElement and its options.

    Parameters
    ----------
    driver: selenium.webdriver.chrome.webdriver.Webdriver
        Selenium driver in a launched state
    element: str or selenium.webdriver.remote.webelement.WebElement
        Element's xpath or element object itself

    Returns
    -------
    Tuple
        Tuple with 2 elements, where first one is select WebElement and second one is a list with all options
    """
    
    select = _get_element_from_input(driver, element)
    select_options = [option.text for option in select.options]
    return select, select_options


def _get_element_from_input(driver: WebDriver, 
                            element: Union[str, WebElement]) -> WebElement:
    if type(element) == WebElement:
        pass
    elif type(element) == str:
        element = driver.find_element_by_xpath(element)
    else:
        raise TypeError(WEB_ELEMENT_INPUT_UNKNOWN_TYPE_MESSAGE)
    return element
    

