"""This module contains ItemDataRetriever class"""

from abc import ABC, abstractmethod

from selenium.webdriver.chrome.webdriver import WebDriver

from . import page_interaction_utils


class BaseItemDataRetriever(ABC):
    """Base class that all ItemDataRetrievers are derived from."""
    @abstractmethod
    def get(self):
        """Method that all ItemDataRetrievers must implement."""
        raise NotImplementedError
    

class ItemDataRetriever(BaseItemDataRetriever):
    r"""Class, which is responsible for retrieving each item's data.

    Attributes
    ----------
    scraping_steps_config: list
        List, where each item represents a scraping step. Every scraping step is a dictionary, where 'execute_action_before_step'
        key corresponds to a function, which is executed before each step and 'elements_info' key corresponds to a dictionary with 
        {element_name: (element_xpath, element_attribute)} format.
    Example:
        def step2_execute_action_before_step(driver):
            needed_url = driver.current_url + '/tab/info'
            driver.get(needed_url)

        scraping_steps_config = [
            {
                'execute_action_before_step': False,
                'elements_info': {
                    'title': ('//h1//span//span', 'textContent'),
                    'category': ('//div[@class="_11eqcnu"]//span', 'href'),
                }
            },
            {
                'execute_action_before_step': step2_execute_action_before_step,
                'elements_info': {
                    'tags': ('//span[@class="_er2xx9"]', 'textContent'),
                    'categories': ('//span[@class="_btwk9a2"]', 'textContent'),
                }
            }
        ]
    
    Methods
    -------
    get(driver)
        Retrieves item's data according to specified scraping_steps_config.
    """

    def __init__(self, scraping_steps_config: list) -> None:
        self.scraping_steps_config = scraping_steps_config

    def get(self, driver: WebDriver):
        r"""Retrieves item's data according to specified scraping_steps_config.
        
        Attributes
        ----------
        driver: selenium.webdriver.chrome.webdriver.Webdriver
            Selenium driver in a launched state

        Returns
        -------
        dict
            A dictionary, where each key represents element_name and each value - scraped information
        """

        data = {}
        for scraping_step_config in self.scraping_steps_config:
            scraped_data = self._perform_scraping_step(driver, scraping_step_config)
            data.update(scraped_data)
        return data
    
    def _perform_scraping_step(self, driver, config):
        execute_action_before_step = config['execute_action_before_step']
        if execute_action_before_step:
            execute_action_before_step(driver)
        
        data = {}
        for element_name, (element_xpath, element_attribute) in config['elements_info'].items():
            data[element_name] = page_interaction_utils.get_elements_data(driver, element_xpath, element_attribute)
        return data
        
    

        
