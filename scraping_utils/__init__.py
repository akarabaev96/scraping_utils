"""Scraping utils automating scraping workflow.

They contain:
- tools automating Selenium driver handling
- tools automating retrieval of items' links
- tools automating retrieval of each item's data
- additional web page interaction utils
- additional utils
"""


from .driver_handler import ChromeDriverHandler
from .item_links_retriever import ItemLinksRetriever
from .item_data_retriever import ItemDataRetriever
from . import page_interaction_utils
from . import utils
from . import decorators