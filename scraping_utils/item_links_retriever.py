"""This module contains ItemLinksRetriever class."""

from abc import ABC, abstractmethod
import random
import time
from typing import Tuple, List

from selenium.webdriver.chrome.webdriver import WebDriver
from tqdm import tqdm

from . import page_interaction_utils


class BaseItemLinksRetriever(ABC):
    """Base class all ItemLinksRetrievers are derived from."""
    @abstractmethod
    def get(self):
        """Method that all ItemLinksRetrievers must implement."""
        raise NotImplementedError
    

class ItemLinksRetriever(BaseItemLinksRetriever):
    r"""Class, which is responsible for retrieving items' links.

    Attributes
    ----------
    item_links_location: Tuple[str, str]
        Tuple, which contains item links' xpath and attribute
    next_page_button_xpath: str
        Xpath of navigate to next page button
    sleep_time_between_iterations: Tuple[int, int], default=(1, 5)
        Specifies a range of wait time between iterations. On each iteration this time will be chosen randomly.
    attempt_count: int, default=None
        Controls how many times try_decorator will be executed. If None, then try_decorator uses default number
        of attempts
    
    Methods
    -------
    get(driver)
        Retrieves items' links.
    """

    def __init__(self,             
                 item_links_location: Tuple[str, str],
                 next_page_button_xpath: str,
                 sleep_time_range_between_iterations: Tuple[int, int] = (1, 5),
                 attempt_count: int = None) -> None:
        self.item_links_location = item_links_location
        self.next_page_button_xpath = next_page_button_xpath
        self.sleep_time_range_between_iterations = sleep_time_range_between_iterations
        self.attempt_count = attempt_count
        

    def get(self, driver: WebDriver) -> List[str]:
        r"""Retrieves items' links. To be more detailed, it does the following:
        - gets all links from current page
        - if "navigate to next page" button is present, then it presses the button and retrieves all items from next as well
        - if "navigate to next page" button is absent, then the function stops.

        Attributes
        ----------
        driver: selenium.webdriver.chrome.webdriver.Webdriver
            Selenium driver in a launched state
        
        Returns
        -------
        list
            List containing all items' links
        """

        progress_bar = tqdm(position=0)
        item_links_xpath, item_links_attribute = self.item_links_location[0], self.item_links_location[1]
        item_links = []
        while True:
            item_links += page_interaction_utils.get_elements_data(driver, item_links_xpath, item_links_attribute, 
                                                                   self.attempt_count)
            next_page_button_existence = page_interaction_utils.check_element_existence(driver, self.next_page_button_xpath)
            if next_page_button_existence == False:
                break
            page_interaction_utils.click_element(driver, self.next_page_button_xpath, self.attempt_count)
            progress_bar.update()
            time.sleep(random.randint(self.sleep_time_range_between_iterations[0], self.sleep_time_range_between_iterations[1]))
        return item_links


                
        
    

    
        
        

        