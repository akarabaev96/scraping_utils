from setuptools import setup

setup(
    name='scraping_utils',
    version='0.1.0',
    description='Custom scraping utils',
    packages=['scraping_utils'],
    install_requires=[
        'selenium',
        'webdriver_manager',
        'tqdm',
        'ipython',
        'bs4',
        'lxml'
    ]
)